--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: ProjetPPE4; Type: SCHEMA; Schema: -; Owner: v.nore
--

CREATE SCHEMA "ProjetPPE4";


ALTER SCHEMA "ProjetPPE4" OWNER TO "v.nore";

SET search_path = "ProjetPPE4", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: personne; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE personne (
    id integer NOT NULL,
    datenaissance date NOT NULL,
    nom character varying(50) NOT NULL,
    prenom character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    telephone character varying(50) NOT NULL,
    entreprise character varying(50),
    login character varying(50),
    password character varying(50)
);


ALTER TABLE personne OWNER TO "v.nore";

--
-- Name: personne_id_seq; Type: SEQUENCE; Schema: ProjetPPE4; Owner: v.nore
--

CREATE SEQUENCE personne_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personne_id_seq OWNER TO "v.nore";

--
-- Name: personne_id_seq; Type: SEQUENCE OWNED BY; Schema: ProjetPPE4; Owner: v.nore
--

ALTER SEQUENCE personne_id_seq OWNED BY appel.id;

--
-- Name: rdv; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE rdv (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    objet character varying(50) NOT NULL,
    commentaire character varying(50)
);


ALTER TABLE rdv OWNER TO "v.nore";

--
-- Name: rdv_id_seq; Type: SEQUENCE; Schema: ProjetPPE4; Owner: v.nore
--

CREATE SEQUENCE rdv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rdv_id_seq OWNER TO "v.nore";

--
-- Name: rdv_id_seq; Type: SEQUENCE OWNED BY; Schema: ProjetPPE4; Owner: v.nore
--

ALTER SEQUENCE rdv_id_seq OWNED BY rdv.id;


--
-- Name: role; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    intitule character varying(15) NOT NULL
);


ALTER TABLE role OWNER TO "v.nore";

--
-- Name: utilisateur; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    nom character varying(50) NOT NULL,
    prenom character varying(50) NOT NULL,
    datenaissance date NOT NULL,
    email character varying(50) NOT NULL,
    telephone character varying(50) NOT NULL,
    numrue integer,
    nomrue character varying(50),
    cp integer,
    ville character varying(50),
    region character varying(50),
    pays character varying(50),
    role integer NOT NULL,
);


ALTER TABLE utilisateur OWNER TO "v.nore";

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: ProjetPPE4; Owner: v.nore
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO "v.nore";

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: ProjetPPE4; Owner: v.nore
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY appel ALTER COLUMN id SET DEFAULT nextval('appel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY note ALTER COLUMN id SET DEFAULT nextval('note_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY rdv ALTER COLUMN id SET DEFAULT nextval('rdv_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: appel; Type: TABLE DATA; Schema: ProjetPPE4; Owner: v.nore
--

INSERT INTO appel VALUES (3, '2017-04-05 16:47:12', 'THOMAS', 'Paul', 'paulthomas@siemens.de', '425871326579', 'WINDJohn', 'BELINDACaroline', 'Commande PGI', 'Commande d un PGI pour le siège sociale', 'Siemens');
INSERT INTO appel VALUES (4, '2014-10-25 09:14:23', 'LERET', 'Christopher', '458789.leretchristopher@disneystudio.com', '825304526', 'BELINDACaroline', 'WINDJohn', 'Devis affiches publicitaires', 'Devis pour des affiches publicitaires', 'DisneyStudio');
INSERT INTO appel VALUES (5, '2017-03-21 16:29:36', 'ROUSSEAU', 'Kevin', 'k.r.rousseau@audi.de', '326987852', 'BELINDACaroline', 'WINDJohn', 'Système d alerte', 'Commande d un système d alerte ', 'Audi Sport');


--
-- Name: appel_id_seq; Type: SEQUENCE SET; Schema: ProjetPPE4; Owner: v.nore
--

SELECT pg_catalog.setval('appel_id_seq', 5, true);


--
-- Data for Name: note; Type: TABLE DATA; Schema: ProjetPPE4; Owner: v.nore
--

INSERT INTO note VALUES (1, '2017-02-14 10:00:58', 2, 2, 'Prendre RDV machin', 'Prendre RDV machin');


--
-- Name: note_id_seq; Type: SEQUENCE SET; Schema: ProjetPPE4; Owner: v.nore
--

SELECT pg_catalog.setval('note_id_seq', 1, true);


--
-- Data for Name: rdv; Type: TABLE DATA; Schema: ProjetPPE4; Owner: v.nore
--

INSERT INTO rdv VALUES (1, '2017-04-03 14:18:26', 'GOHNS', 'Carlos', 'carlos.gohns@renault.com', '298784523', 'WINDJohn', 'Commande nouveau logiciel', 35, 'Rue des marrons', '', '8', 26, 'Valence', 'Drôme', 'France', '', 'BELINDACaroline', 'Renault');


--
-- Name: rdv_id_seq; Type: SEQUENCE SET; Schema: ProjetPPE4; Owner: v.nore
--

SELECT pg_catalog.setval('rdv_id_seq', 2, true);


--
-- Data for Name: role; Type: TABLE DATA; Schema: ProjetPPE4; Owner: v.nore
--

INSERT INTO role VALUES (1, 'Administrateur');
INSERT INTO role VALUES (2, 'Utilisateur');


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: ProjetPPE4; Owner: v.nore
--

INSERT INTO utilisateur VALUES (2, 'BELINDA', 'Caroline', '1974-04-15', 'cbelinda@amazonfr.com', '0639781234', 15, 'Boulevard Clovi', 75, 'Paris', 'Ile de France', 'France', 1, 'c.belinda', 'P@ssword');
INSERT INTO utilisateur VALUES (1, 'WIND', 'John', '1961-11-04', 'jwind@amazonfr.com', '0158672147', 15, 'Boulevard Clovis', 75, 'Paris', 'Ile de France', 'France', 2, 'j.wind', 'P@ssword');


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: ProjetPPE4; Owner: v.nore
--

SELECT pg_catalog.setval('utilisateur_id_seq', 2, true);


--
-- Name: appel_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY appel
    ADD CONSTRAINT appel_pkey PRIMARY KEY (id);


--
-- Name: note_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY note
    ADD CONSTRAINT note_pkey PRIMARY KEY (id);


--
-- Name: rdv_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY rdv
    ADD CONSTRAINT rdv_pkey PRIMARY KEY (id);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: fk_utilisateur_note; Type: FK CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY note
    ADD CONSTRAINT fk_utilisateur_note FOREIGN KEY (utilisateur) REFERENCES utilisateur(id);


--
-- Name: fk_utilisateur_note2; Type: FK CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY note
    ADD CONSTRAINT fk_utilisateur_note2 FOREIGN KEY (concerner) REFERENCES utilisateur(id);


--
-- Name: fk_utilisateur_role; Type: FK CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT fk_utilisateur_role FOREIGN KEY (role) REFERENCES role(id);


--
-- Name: ProjetPPE4; Type: ACL; Schema: -; Owner: v.nore
--

REVOKE ALL ON SCHEMA "ProjetPPE4" FROM PUBLIC;
REVOKE ALL ON SCHEMA "ProjetPPE4" FROM "v.nore";
GRANT ALL ON SCHEMA "ProjetPPE4" TO "v.nore";
GRANT ALL ON SCHEMA "ProjetPPE4" TO uservincent;


--
-- Name: appel; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON TABLE appel FROM PUBLIC;
REVOKE ALL ON TABLE appel FROM "v.nore";
GRANT ALL ON TABLE appel TO "v.nore";
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE appel TO uservincent;


--
-- Name: appel.id; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL(id) ON TABLE appel FROM PUBLIC;
REVOKE ALL(id) ON TABLE appel FROM "v.nore";
GRANT ALL(id) ON TABLE appel TO uservincent;


--
-- Name: appel_id_seq; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON SEQUENCE appel_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE appel_id_seq FROM "v.nore";
GRANT ALL ON SEQUENCE appel_id_seq TO "v.nore";
GRANT ALL ON SEQUENCE appel_id_seq TO uservincent;


--
-- Name: note; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON TABLE note FROM PUBLIC;
REVOKE ALL ON TABLE note FROM "v.nore";
GRANT ALL ON TABLE note TO "v.nore";
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE note TO uservincent;


--
-- Name: note_id_seq; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON SEQUENCE note_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE note_id_seq FROM "v.nore";
GRANT ALL ON SEQUENCE note_id_seq TO "v.nore";
GRANT ALL ON SEQUENCE note_id_seq TO uservincent;


--
-- Name: rdv; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON TABLE rdv FROM PUBLIC;
REVOKE ALL ON TABLE rdv FROM "v.nore";
GRANT ALL ON TABLE rdv TO "v.nore";
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rdv TO uservincent;


--
-- Name: rdv_id_seq; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON SEQUENCE rdv_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE rdv_id_seq FROM "v.nore";
GRANT ALL ON SEQUENCE rdv_id_seq TO "v.nore";
GRANT ALL ON SEQUENCE rdv_id_seq TO uservincent;


--
-- Name: utilisateur; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON TABLE utilisateur FROM PUBLIC;
REVOKE ALL ON TABLE utilisateur FROM "v.nore";
GRANT ALL ON TABLE utilisateur TO "v.nore";
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE utilisateur TO uservincent;


--
-- Name: utilisateur_id_seq; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON SEQUENCE utilisateur_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE utilisateur_id_seq FROM "v.nore";
GRANT ALL ON SEQUENCE utilisateur_id_seq TO "v.nore";
GRANT ALL ON SEQUENCE utilisateur_id_seq TO uservincent;


--
-- PostgreSQL database dump complete
--

