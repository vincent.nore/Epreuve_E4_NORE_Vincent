--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: ProjetPPE4; Type: SCHEMA; Schema: -; Owner: v.nore
--

CREATE SCHEMA "ProjetPPE4";


ALTER SCHEMA "ProjetPPE4" OWNER TO "v.nore";

SET search_path = "ProjetPPE4", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: personne; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE personne (
    id integer NOT NULL,
    datenaissance date NOT NULL,
    nom character varying(50) NOT NULL,
    prenom character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    telephone character varying(50) NOT NULL,
    entreprise character varying(50),
    login character varying(50),
    password character varying(50)
);


ALTER TABLE personne OWNER TO "v.nore";

--
-- Name: personne_id_seq; Type: SEQUENCE; Schema: ProjetPPE4; Owner: v.nore
--

CREATE SEQUENCE personne_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personne_id_seq OWNER TO "v.nore";

--
-- Name: personne_id_seq; Type: SEQUENCE OWNED BY; Schema: ProjetPPE4; Owner: v.nore
--

ALTER SEQUENCE personne_id_seq OWNED BY personne.id;

--
-- Name: rdv; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE rdv (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    objet character varying(50) NOT NULL,
    commentaire character varying(50)
);


ALTER TABLE rdv OWNER TO "v.nore";

--
-- Name: rdv_id_seq; Type: SEQUENCE; Schema: ProjetPPE4; Owner: v.nore
--

CREATE SEQUENCE rdv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rdv_id_seq OWNER TO "v.nore";

--
-- Name: rdv_id_seq; Type: SEQUENCE OWNED BY; Schema: ProjetPPE4; Owner: v.nore
--

ALTER SEQUENCE rdv_id_seq OWNED BY rdv.id;


--
-- Name: role; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    intitule character varying(15) NOT NULL
);


ALTER TABLE role OWNER TO "v.nore";

--
-- Name: lieu; Type: TABLE; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

CREATE TABLE lieu (
    id integer NOT NULL,
    numrue integer,
    nomrue character varying(50),
    batiment character varying(50),
    etage character varying(50),
    cp integer,
    ville character varying(50),
    region character varying(50),
    pays character varying(50),
);


ALTER TABLE lieu OWNER TO "v.nore";

--
-- Name: lieu_id_seq; Type: SEQUENCE; Schema: ProjetPPE4; Owner: v.nore
--

CREATE SEQUENCE lieu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE lieu_id_seq OWNER TO "v.nore";

--
-- Name: lieu_id_seq; Type: SEQUENCE OWNED BY; Schema: ProjetPPE4; Owner: v.nore
--

ALTER SEQUENCE lieu_id_seq OWNED BY lieu.id;


--
-- Name: id; Type: DEFAULT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY personne ALTER COLUMN id SET DEFAULT nextval('personne_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY rdv ALTER COLUMN id SET DEFAULT nextval('rdv_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY lieu ALTER COLUMN id SET DEFAULT nextval('lieu_id_seq'::regclass);


SELECT pg_catalog.setval('personne_id_seq', 5, true);


--
-- Name: note_id_seq; Type: SEQUENCE SET; Schema: ProjetPPE4; Owner: v.nore
--

SELECT pg_catalog.setval('note_id_seq', 1, true);


--
-- Name: rdv_id_seq; Type: SEQUENCE SET; Schema: ProjetPPE4; Owner: v.nore
--

SELECT pg_catalog.setval('rdv_id_seq', 2, true);


--
-- Name: lieu_id_seq; Type: SEQUENCE SET; Schema: ProjetPPE4; Owner: v.nore
--

SELECT pg_catalog.setval('lieu_id_seq', 2, true);


--
-- Name: personne_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY personne
    ADD CONSTRAINT personne_pkey PRIMARY KEY (id);


--
-- Name: note_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY note
    ADD CONSTRAINT note_pkey PRIMARY KEY (id);


--
-- Name: rdv_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY rdv
    ADD CONSTRAINT rdv_pkey PRIMARY KEY (id);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: lieu_pkey; Type: CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore; Tablespace: 
--

ALTER TABLE ONLY lieu
    ADD CONSTRAINT lieu_pkey PRIMARY KEY (id);


--
-- Name: fk_lieu_note; Type: FK CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY note
    ADD CONSTRAINT fk_lieu_note FOREIGN KEY (lieu) REFERENCES lieu(id);


--
-- Name: fk_lieu_note2; Type: FK CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY note
    ADD CONSTRAINT fk_lieu_note2 FOREIGN KEY (concerner) REFERENCES lieu(id);


--
-- Name: fk_lieu_role; Type: FK CONSTRAINT; Schema: ProjetPPE4; Owner: v.nore
--

ALTER TABLE ONLY lieu
    ADD CONSTRAINT fk_lieu_role FOREIGN KEY (role) REFERENCES role(id);


--
-- Name: ProjetPPE4; Type: ACL; Schema: -; Owner: v.nore
--

REVOKE ALL ON SCHEMA "ProjetPPE4" FROM PUBLIC;
REVOKE ALL ON SCHEMA "ProjetPPE4" FROM "v.nore";
GRANT ALL ON SCHEMA "ProjetPPE4" TO "v.nore";
GRANT ALL ON SCHEMA "ProjetPPE4" TO uservincent;


--
-- Name: personne; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON TABLE personne FROM PUBLIC;
REVOKE ALL ON TABLE personne FROM "v.nore";
GRANT ALL ON TABLE personne TO "v.nore";
GRANT SELECT,INSERT,REFERENCES,DELETE,TRIGGER,UPDATE ON TABLE personne TO uservincent;


--
-- Name: personne.id; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL(id) ON TABLE personne FROM PUBLIC;
REVOKE ALL(id) ON TABLE personne FROM "v.nore";
GRANT ALL(id) ON TABLE personne TO uservincent;


--
-- Name: personne_id_seq; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON SEQUENCE personne_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE personne_id_seq FROM "v.nore";
GRANT ALL ON SEQUENCE personne_id_seq TO "v.nore";
GRANT ALL ON SEQUENCE personne_id_seq TO uservincent;


--
-- Name: rdv; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON TABLE rdv FROM PUBLIC;
REVOKE ALL ON TABLE rdv FROM "v.nore";
GRANT ALL ON TABLE rdv TO "v.nore";
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rdv TO uservincent;


--
-- Name: rdv_id_seq; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON SEQUENCE rdv_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE rdv_id_seq FROM "v.nore";
GRANT ALL ON SEQUENCE rdv_id_seq TO "v.nore";
GRANT ALL ON SEQUENCE rdv_id_seq TO uservincent;


--
-- Name: lieu; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON TABLE lieu FROM PUBLIC;
REVOKE ALL ON TABLE lieu FROM "v.nore";
GRANT ALL ON TABLE lieu TO "v.nore";
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE lieu TO uservincent;


--
-- Name: lieu_id_seq; Type: ACL; Schema: ProjetPPE4; Owner: v.nore
--

REVOKE ALL ON SEQUENCE lieu_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE lieu_id_seq FROM "v.nore";
GRANT ALL ON SEQUENCE lieu_id_seq TO "v.nore";
GRANT ALL ON SEQUENCE lieu_id_seq TO uservincent;


--
-- PostgreSQL database dump complete
--

